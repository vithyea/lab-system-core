<?php

namespace App\Http\Controllers\Auth;

use App\Models\Menu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function user()
    {

        $user = auth()->user();

        return json_data($user);
    }
}
